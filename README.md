**Spring Rest audit log sample with NOSQL and Message Broker**

This is an example of a implementation of a service rest in Spring with mongoDB database and ActiveMq message broker. This Service 
rest is for a small audit logs aplication

* For begginer with NOSQL I recommend that you open, in another tab,  [watch MondoDB video](https://www.mongodb.com/blog/post/start-here-a-video-introduction-to-mongodb-stitch) . for begginer with activeMQ i Recomend to see the [ActiveMQ video](https://www.youtube.com/watch?v=oaegBVoVvlQ) .*

---

## Generating  Rest Service project

This project was created from scracth using Sring boot initializer in [Spring Initializr](https://start.spring.io/).

1. Choose **build management** maven project.
2. Choose language **Java**.
3. write you **group** and **artifact**.
4. Choose the  *Spring dependencies.*
5. Click **Generate** .

---

## Workin with Active MQ

To start using activeMQ i have done:
1. added depedency: 

```
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-activemq</artifactId>
		</dependency>
```

2. Added Spring configuration to retrieve **ActiveMQConnectionFactory**.


3. Added a message to JSON converter-**JsonMessageConverter** class.

4. Adder  **ActionMapper**  classe and **UserDTO** to serialize to message to Activem MQ.
5. added timezone configuration *for LocalDate to JSON convert to put message in queue*.

6. Implemented a listener to get message from queue and insert into MongoDB database.




## Working with MongoDB

To start using activeMQ i have done:
1. added depedency: 

````
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-mongodb</artifactId>
		</dependency>
````

2. operation with MongoDB was madded by autowiring  **UserRepository** interface.
	



## Implemented Rest Service

there was impemented the methods:

1. POST
2. PUT
3. DELETE
4. GET

*It was added Pagination and sorting to the GET requests.*

*Sensible Data was masked.*

## Rest Service documentation

Documentation was made with OpenAPI Swagger specification v2.
the documentation is aceded locaby by : http://localhost:8081/swagger-ui.html

