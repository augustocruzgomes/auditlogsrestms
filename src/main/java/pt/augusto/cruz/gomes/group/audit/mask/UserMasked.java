package pt.augusto.cruz.gomes.group.audit.mask;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import pt.augusto.cruz.gomes.group.audit.domain.Action;

public class UserMasked {
	
	@Id
	private String userid;
	private String name;	
	private String crediCard;
	private List<Action> actions;
	private String email;
	
//	@JsonFormat(pattern="dd-MM-yyyy")     
//	private Date creationDate;
	@JsonFormat(pattern="yyyy-MM-dd") 	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate  creationDate;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCrediCard() {
		return crediCard;
	}

	public void setCrediCard(String crediCard) {
		this.crediCard = crediCard;
	}

	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
