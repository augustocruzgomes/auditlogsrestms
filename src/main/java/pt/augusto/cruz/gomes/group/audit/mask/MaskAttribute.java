package pt.augusto.cruz.gomes.group.audit.mask;

public interface MaskAttribute {
	
	public String mask(String input, String maskPattern);

}
