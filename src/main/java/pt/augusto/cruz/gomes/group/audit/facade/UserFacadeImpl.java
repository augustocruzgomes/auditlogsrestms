package pt.augusto.cruz.gomes.group.audit.facade;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.jms.Queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import pt.augusto.cruz.gomes.group.audit.Exception.MyResourceNotFoundException;
import pt.augusto.cruz.gomes.group.audit.converter.UserConverter;
import pt.augusto.cruz.gomes.group.audit.domain.User;
import pt.augusto.cruz.gomes.group.audit.dto.UserDTO;
import pt.augusto.cruz.gomes.group.audit.mapper.UserToJSONMapper;
import pt.augusto.cruz.gomes.group.audit.mask.MaskAuditService;
import pt.augusto.cruz.gomes.group.audit.mask.UserMasked;
import pt.augusto.cruz.gomes.group.audit.mongo.dao.UserDAO;

@Service
public class UserFacadeImpl implements UserFacade {

	@Autowired
	JmsTemplate jmsTemplate;

	@Autowired
	Queue queue;

	@Autowired
	MaskAuditService maskAuditService;

	private final UserDAO repository;

	@Autowired
	UserFacadeImpl(@Qualifier("userDAO") UserDAO repository) {
		this.repository = repository;
	}

	@Override
	public void create(UserDTO userDTO) throws ResponseStatusException {
		System.out.println("Sending a user transaction.");

		User userInDB = repository.findByCrediCard(userDTO.getCrediCard());
		if (userInDB == null) {
			UserToJSONMapper mapper = new UserToJSONMapper();
			String objJSON = mapper.readJsonfromUserMapper(userDTO);
			jmsTemplate.convertAndSend(queue, objJSON);

			System.out.println("json ok.");
		} else {

			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Utilizador ja existe", null);
		}

	}

	@Override
	public UserMasked delete(String id) throws NoSuchElementException {
		System.out.println("deleting  a user transaction.");		

		Optional<User> userInDB = null;
		userInDB = repository.findById(id);

		if (userInDB.get() != null)
			repository.deleteById(id);
		else
			throw new NoSuchElementException();

		return maskAuditService.mask(userInDB.get());
	}

	@Override
	public Page<UserMasked> findAll(Pageable pageable) throws MyResourceNotFoundException {		
		Page<User> resultPage = null;
		Page<UserMasked> maskedDTOs;
		try {

			Pageable sortedByName = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("name"));
			resultPage = repository.findAll(sortedByName);			
			maskedDTOs  = resultPage.map(maskAuditService::mask);		   

			if (pageable.getPageNumber() > resultPage.getTotalPages()) {
				throw new MyResourceNotFoundException();
			}
		}catch (RuntimeException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Erro desconhecido", e);
		}
		return maskedDTOs;
	}

	@Override
	public Page<UserMasked> findUsersByDateBetween(LocalDate DateIni, LocalDate EndDate, Pageable pageable) {
		List<UserMasked> maskedUsers = new ArrayList<UserMasked>();

		Page<User> users = repository.findUsersByDateBetween(DateIni, EndDate, pageable);

		for (User user : users) {
			UserMasked userMasked = maskAuditService.mask(user);
			maskedUsers.add(userMasked);
		}
		
		Page<UserMasked> maskedDTOs = users.map(maskAuditService::mask);

		return maskedDTOs;
	}

	@Override
	public UserMasked findById(String id) {
		Optional<User> user = repository.findById(id);
		return maskAuditService.mask(user.get());
	}

	@Override
	public UserMasked update(UserDTO userDTO) throws NoSuchElementException, ResponseStatusException {
		System.out.println("Sending a user transaction.");
		UserMasked userMasked = null;
		try {

			Optional<User> userInDB = repository.findById(userDTO.getUserid());

			UserConverter userConverter = new UserConverter();
			User user = userConverter.UpdateUser(userInDB.get(), userDTO);

			repository.save(user);
			String userID = user.getUserid();
			// get the updated object again
			Optional<User> userTest1_1 = repository.findById(userID);

			System.out.println("userTest1_1 - " + userTest1_1.get());

			System.out.println("update ok.");

			userMasked = maskAuditService.mask(user);

		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilizador não encontrado", e);

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "erro desconhecido", e);
		}
		return userMasked;
	}

	@Override
	public Page<UserMasked> findAll(int page, int size) {		
		Page<User> resultPage = null;
		Page<UserMasked> maskedDTOs;
		try {

			Pageable sortedByName = PageRequest.of(page, size, Sort.by("name"));
			resultPage = repository.findAll(sortedByName);			
			maskedDTOs = resultPage.map(maskAuditService::mask);

			if (page > resultPage.getTotalPages()) {
				throw new MyResourceNotFoundException();
			}		
		} catch (RuntimeException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Erro desconhecido", e);
		}
		return maskedDTOs;
	}

}
