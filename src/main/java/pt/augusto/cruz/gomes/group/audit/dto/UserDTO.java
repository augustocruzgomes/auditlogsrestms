package pt.augusto.cruz.gomes.group.audit.dto;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document
@Getter
@Setter
@NoArgsConstructor
public class UserDTO {
	
	//@Id
	private String userid;
	private String name;
	private String password;
	private String token;
	private String crediCard;

	@JsonFormat(pattern="yyyy-MM-dd") 
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate  creationDate;
	private List<ActionDTO> actions;
	private String email;
	
	public UserDTO(final String userid, final String name, final String password, final String token, final String crediCard, final LocalDate creationDate, final List<ActionDTO> actions, final String email) {
		this.actions = actions;
		this.name = name;		
		this.password = password;
		this.token = token;
		this.crediCard = crediCard;
		this.creationDate = creationDate;
		this.email = email;		
	}	

	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getCrediCard() {
		return crediCard;
	}
	public void setCrediCard(String crediCard) {
		this.crediCard = crediCard;
	}
	public List<ActionDTO> getActions() {
		return actions;
	}
	public void setActions(List<ActionDTO> actions) {
		this.actions = actions;
	}


	public LocalDate getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	








	


	


	


	
	
	
	

}
