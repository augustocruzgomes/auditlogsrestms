package pt.augusto.cruz.gomes.group.audit.converter;

import pt.augusto.cruz.gomes.group.audit.domain.User;
import pt.augusto.cruz.gomes.group.audit.dto.UserDTO;

public class UserConverter {

	
	public User UpdateUser(User userIn, UserDTO userDTO) {
		
		if(userDTO.getUserid() != null)
			userIn.setUserid(userDTO.getUserid());
		userIn.setName(userDTO.getName());
		userIn.setCrediCard(userDTO.getCrediCard());
		userIn.setPassword(userDTO.getPassword());
		userIn.setToken(userDTO.getToken());
		ActionsConverter actionsConverter = new ActionsConverter();
		userIn.setActions(actionsConverter.convertActionDTOToUpdateDB(userDTO.getActions()));
		userIn.setEmail(userIn.getEmail());
		return userIn;

	}
	
	public User convertUserToUpdateDB(User userIn) {
		User userOut = new User();
		if(userIn.getUserid() != null)
			userOut.setUserid(userIn.getUserid());
		userOut.setName(userIn.getName());
		userOut.setCrediCard(userIn.getCrediCard());
		userOut.setPassword(userIn.getPassword());
		userOut.setToken(userIn.getToken());
		ActionsConverter actionsConverter = new ActionsConverter();
		userOut.setActions(actionsConverter.convertActionsToUpdateDB(userIn.getActions()));
		userOut.setEmail(userIn.getEmail());
		return userOut;
	}
	
	
	
	public User convertUserDTOToEntity(UserDTO userIn) {
		User userOut = new User();
		if(userIn.getUserid() != null)
			userOut.setUserid(userIn.getUserid());
		userOut.setName(userIn.getName());
		userOut.setCrediCard(userIn.getCrediCard());
		userOut.setPassword(userIn.getPassword());
		userOut.setToken(userIn.getToken());
		userOut.setCreationDate(userIn.getCreationDate());
		ActionsConverter actionsConverter = new ActionsConverter();
		userOut.setActions(actionsConverter.convertActionTransactionsToUpdateDB(userIn.getActions()));
		userOut.setEmail(userIn.getEmail());

		return userOut;

	}
	
	
	public UserDTO convertUserToDTO(User userIn) {
	
		ActionsConverter actionsConverter = new ActionsConverter();
		UserDTO userOut = new UserDTO(userIn.getUserid(), userIn.getName(), userIn.getPassword(),
				userIn.getToken(), userIn.getCrediCard(), userIn.getCreationDate(), 
				actionsConverter.convertActionToDTO(userIn.getActions()), userIn.getEmail() );
		
		return userOut;
	}
	

}
