package pt.augusto.cruz.gomes.group.audit.facade;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import pt.augusto.cruz.gomes.group.audit.dto.UserDTO;
import pt.augusto.cruz.gomes.group.audit.mask.UserMasked;

public interface UserFacade {
	
	void create(UserDTO userDTO);
	 
	UserMasked delete(String id);
 
	Page<UserMasked> findAll(Pageable pageable);
    
	Page<UserMasked> findAll(int page, int size);
    
   // List<UserMasked> findUsersByDateBetween(LocalDate DateIni, LocalDate EndDate, Pageable pageable);
    
    Page<UserMasked> findUsersByDateBetween(LocalDate DateIni, LocalDate EndDate, Pageable pageable);
 
    UserMasked findById(String id);
 
    UserMasked update(UserDTO userDTO);

}
