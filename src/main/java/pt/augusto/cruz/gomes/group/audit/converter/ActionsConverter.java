package pt.augusto.cruz.gomes.group.audit.converter;

import java.util.ArrayList;
import java.util.List;

import pt.augusto.cruz.gomes.group.audit.domain.Action;
import pt.augusto.cruz.gomes.group.audit.dto.ActionDTO;

public class ActionsConverter {
	
	
	
	public List<Action> convertActionDTOToUpdateDB(List<ActionDTO> actionsIn) {
		List<Action> actionsOut = new ArrayList<Action>();

		for (ActionDTO actionIn : actionsIn) {
			Action actionOut = new Action();
			if (actionIn.getId() != null)
				actionOut.setId(actionIn.getId());

			actionOut.setDate(actionIn.getDate());
			actionOut.setWhat(actionIn.getWhat());
			actionsOut.add(actionOut);
		}
		
//		  return actionsIn.stream()
//	                .map(this::convertToAction)
//	                .collect(toList());
//		
		
		
		return actionsOut;
	}
	

	public List<Action> convertActionsToUpdateDB(List<Action> actionsIn) {
		List<Action> actionsOut = new ArrayList<Action>();

		for (Action actionIn : actionsIn) {
			Action actionOut = new Action();
			if (actionIn.getId() != null)
				actionOut.setId(actionIn.getId());

			actionOut.setDate(actionIn.getDate());
			actionOut.setWhat(actionIn.getWhat());
			actionsOut.add(actionOut);

		}
		return actionsOut;
	}
	
	
	
	public List<Action> convertActionTransactionsToUpdateDB(List<ActionDTO> actionsIn) {
		List<Action> actionsOut = new ArrayList<Action>();

		for (ActionDTO actionIn : actionsIn) {
			Action actionOut = new Action();
			if (actionIn.getId() != null)
				actionOut.setId(actionIn.getId());

			actionOut.setDate(actionIn.getDate());
			actionOut.setWhat(actionIn.getWhat());
			actionsOut.add(actionOut);

		}
		return actionsOut;
	}
	
	
	public List<ActionDTO> convertActionToDTO(List<Action> actionsIn) {
		List<ActionDTO> actionsOut = new ArrayList<ActionDTO>();

		for (Action actionIn : actionsIn) {
			ActionDTO actionOut = new ActionDTO(actionIn.getId(), actionIn.getDate(), actionIn.getWhat());

			actionsOut.add(actionOut);

		}
		return actionsOut;
	}


}
