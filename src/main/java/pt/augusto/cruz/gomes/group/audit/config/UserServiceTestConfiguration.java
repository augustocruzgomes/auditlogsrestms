package pt.augusto.cruz.gomes.group.audit.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import pt.augusto.cruz.gomes.group.audit.facade.UserFacadeImpl;

@Profile("test")
@Configuration
public class UserServiceTestConfiguration {
    @Bean
    @Primary
    public UserFacadeImpl nameService() {
        return Mockito.mock(UserFacadeImpl.class);
    }
}
