package pt.augusto.cruz.gomes.group.audit.mongo.dao;


import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import pt.augusto.cruz.gomes.group.audit.domain.User;

public interface UserDAO extends MongoRepository<User, String> {
	
	public User findByName(String name);
	public List<User> findUsersByCrediCard(String crediCard);
	public User findByCrediCard(String crediCard);
	
//	@Query("{ 'creationDate' : { $gt: ?0, $lt: ?1 } }")
//	List<User> findUsersByDateBetween(Instant  dateGT, Instant endDate);
	
	@Query("{ 'creationDate' : { $gt: ?0, $lt: ?1 } }")
	Page<User> findUsersByDateBetween(LocalDate  dateGT, LocalDate endDate, Pageable page);
	
	
//	@Bean public  default MongoTemplate mongoTemplate() throws Exception 
//	{ 
//	  return new MongoTemplate(mongoClient(), "database_name");
//	}
//	
//	public default MongoClient mongoClient() { 
//		
//		
//		return new MongoClientImpl("127.0.0.1", 27017); 
//		} 

}

