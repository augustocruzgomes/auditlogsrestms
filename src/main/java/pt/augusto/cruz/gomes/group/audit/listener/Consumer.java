package pt.augusto.cruz.gomes.group.audit.listener;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import pt.augusto.cruz.gomes.group.audit.converter.UserConverter;
import pt.augusto.cruz.gomes.group.audit.domain.User;
import pt.augusto.cruz.gomes.group.audit.dto.UserDTO;
import pt.augusto.cruz.gomes.group.audit.mapper.UserToJSONMapper;
import pt.augusto.cruz.gomes.group.audit.mongo.dao.UserDAO;



@Component
public class Consumer {	


	
	@Autowired
	private UserDAO userRepository;

	@JmsListener(destination = "actions.queue")
	public void consume(String message) {

		UserToJSONMapper mapper = new UserToJSONMapper();

		UserDTO obj = mapper.fromJsontoUserTransaction(message);

		System.out.println("Received from activemq: " + obj.toString());			
		
		UserConverter userConverter = new UserConverter();			
		
		User user = userConverter.convertUserDTOToEntity(obj);

		userRepository.save(user);

		List<User> users = userRepository.findAll();

		for (User u : users)
			System.out.println("User: " + u.getName());

	}

}
