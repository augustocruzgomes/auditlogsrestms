package pt.augusto.cruz.gomes.group.audit.mapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import pt.augusto.cruz.gomes.group.audit.dto.ActionDTO;
import pt.augusto.cruz.gomes.group.audit.dto.UserDTO;

public class UserToJSONMapper {
	
	
	
	
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private  ObjectMapper objectMapper;

    public UserToJSONMapper() {
    	objectMapper = new ObjectMapper();

    	// configuration for LocalDate to JSON convert to put message in queue    	
    	JavaTimeModule timeModule = new JavaTimeModule();
        timeModule.addSerializer(LocalDate.class, 
            new LocalDateSerializer(DateTimeFormatter.ISO_LOCAL_DATE));
        timeModule.addSerializer(LocalDateTime.class, 
            new LocalDateTimeSerializer(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        objectMapper.registerModule(timeModule);
    	
    }
    
    
    public String readJsonWithObjectMapper(UserDTO  obj)  {
		
		String json = "";
		try {
			json = objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return json;
	}
    
    
    public String readJsonfromUserMapper(UserDTO  obj)  {
		
		String json = "";
		try {
			json = objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return json;
	}
    
    
    public ActionDTO fromJsontoActionTransaction(String  json)  {
	  
	   	ActionDTO objAction = null;
	   	
	   	
	   	try {
			objAction = objectMapper.readValue(json, ActionDTO.class);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	   	
	   	return objAction;
   }
    
    public UserDTO fromJsontoUserTransaction(String  json)  {
	   
	   	UserDTO objAction = null;
	   	
	   	
	   	try {
			objAction = objectMapper.readValue(json, UserDTO.class);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	   	
	   	return objAction;
  }
        
   

}
