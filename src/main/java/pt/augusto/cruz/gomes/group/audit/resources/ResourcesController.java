package pt.augusto.cruz.gomes.group.audit.resources;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import pt.augusto.cruz.gomes.group.audit.Exception.MyResourceNotFoundException;
import pt.augusto.cruz.gomes.group.audit.dto.UserDTO;
import pt.augusto.cruz.gomes.group.audit.facade.UserFacade;
import pt.augusto.cruz.gomes.group.audit.mask.UserMasked;
import io.swagger.annotations.ApiResponse;

//@Slf4j
@RestController
@RequestMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
@Validated
public class ResourcesController {

	private final UserFacade service;

	@Autowired
	ResourcesController(@Qualifier("userFacadeImpl") UserFacade service) {
		this.service = service;
	}

	
	@ResponseBody
    @ResponseStatus(value = HttpStatus.CREATED)
	@RequestMapping(value = "/v1/users", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE}, 
			consumes = {MediaType.APPLICATION_JSON_VALUE}	)
	@ApiOperation(value = "Create new or update existing audit logs", 
		notes = "Creates new or updates exisiting person audit logs. Returns Creation Status", 
		response = String.class)
	public ResponseEntity<String> sendUser(@ApiParam("userDTO") @Valid @RequestBody UserDTO userDTO) {
		try {
			service.create(userDTO);
		} catch (ResponseStatusException e) {
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Utilizador Já existe", null);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Erro desconhecido", e);
		}
		return new ResponseEntity<String>("user inserted sucessfuly", HttpStatus.CREATED);
	}

	@ResponseBody
    @ResponseStatus(value = HttpStatus.ACCEPTED)
	@RequestMapping(value = "/v1/users", method = RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE}, 
			consumes = {MediaType.APPLICATION_JSON_VALUE}	)
	@ApiOperation(value = "Update new or update existing person", 
		notes = "Updates new or updates exisitng person. Returns created/updated person with id.", 
		response = UserMasked.class)
	public ResponseEntity<UserMasked> update(@ApiParam("userDTO") @Valid @RequestBody UserDTO userDTO) {

		UserMasked userMasked = null;
		try {
			userMasked = service.update(userDTO);

		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilizador não encontrado", e);

		} catch (ResponseStatusException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilizador não encontrado", e);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Erro desconhecido", e);
		}

		return new ResponseEntity<UserMasked>(userMasked, HttpStatus.ACCEPTED);
	}

	
	@ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/v1/users/{id}", method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE}, 
			consumes = {MediaType.APPLICATION_JSON_VALUE}	)
	@ApiOperation(value = "Delete new or update existing person", 
		notes = "Deletes new or updates exisitng person. Returns created/updated person with id.", 
		response = UserMasked.class)
	@ApiImplicitParam(dataType = "string", name = "id") 
	public ResponseEntity<UserMasked> delete(@ApiParam(type = "string", name = "id") @PathVariable String id) {
		UserMasked userMasked = null;
		try {
			userMasked = service.delete(id);

		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilizador não encontrado", e);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<UserMasked>(userMasked, HttpStatus.OK);
	}

	@RequestMapping(value = "/v1/users", method = RequestMethod.GET)
	@ApiOperation(value = "Get all persons", 
		notes = "Returns first N persons specified by the size parameter with page offset specified by page parameter.",
		response = Page.class,
		produces = MediaType.APPLICATION_JSON_VALUE)
	public Page<UserMasked> usersBYDateIntervals(
			@ApiParam(type = "date", name = "DateIni") @RequestParam("DateIni") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate DateIni,
			@ApiParam(type = "date", name = "EndDate") @RequestParam("EndDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate EndDate,
			@ApiParam("pageable") Pageable pageable, @ApiParam(type = "integer", name = "page", defaultValue = "0")  @RequestParam("page") int page,
			@ApiParam(type = "integer", name = "size", defaultValue = "0")  @RequestParam("size") int size) {

		return service.findUsersByDateBetween(DateIni, EndDate, pageable);
	}

	@RequestMapping(value = "/v1/users/{userid}", method = RequestMethod.GET)
	@ApiOperation(value = "Get person by id", 
		notes = "Returns person for id specified.",
		response = UserMasked.class,
		produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Person not found") })
	@ApiImplicitParam(dataType = "string", name = "userid")
	public UserMasked userid(@ApiParam("userid") @PathVariable("userid") final String userid) {
		return service.findById(userid);
	}

	@RequestMapping(value = "/v1/usersByName", method = RequestMethod.GET, params = { "page", "size" })
	@ApiOperation(value = "Get person by id", 
		notes = "Returns person for id specified.", 
		response = Page.class,
		produces = MediaType.APPLICATION_JSON_VALUE)
	public Page<UserMasked> findPaginatedOrderedByName(@ApiParam(type = "integer", name = "page", defaultValue = "0") @RequestParam("page") int page,
			@ApiParam(type = "integer", name = "size", defaultValue = "0") @RequestParam("size") int size, UriComponentsBuilder uriBuilder,
			HttpServletResponse response) {
		Page<UserMasked> maskedUsers = null;
		try {
			maskedUsers = service.findAll(page, size);
		} catch (MyResourceNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilizadores não encontrados", e);

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "erro desconhecido", e);
		}
		return maskedUsers;
	}

}
