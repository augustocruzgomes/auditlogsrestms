package pt.augusto.cruz.gomes.group.audit.unit;



import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.Contains;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import pt.augusto.cruz.gomes.group.audit.domain.User;
import pt.augusto.cruz.gomes.group.audit.mask.MaskAuditService;
import pt.augusto.cruz.gomes.group.audit.mask.UserMasked;
import org.springframework.test.context.junit4.SpringRunner;
import static org.hamcrest.CoreMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class MaskedUserTest {
	
	@Autowired
	MaskAuditService maskAuditService;
	
	@Test
	public void maskCreditCardTest() {
		// given
		User user = new User();
		user.setName("Teste Name");
		user.setCrediCard("1000027");
	
		//When
		UserMasked userMasked = maskAuditService.mask(user);
		
		String maskedResult = "1x-xxx27";
		
		// then	
		Assert.assertEquals(userMasked.getCrediCard(), maskedResult);  
		
	}
	
	
	
	@Test
	public void maskEmailTest() {
		// given
		User user = new User();
		user.setName("Teste Name");
		user.setCrediCard("1000027");
		user.setEmail("email.teste@teste.pt");
	
		//When
		UserMasked userMasked = maskAuditService.mask(user);		
		
		
		// then	
		String maskedResult = "e*********e@teste.pt";
		Assert.assertThat(userMasked.getEmail(), containsString(maskedResult)); 
		 
		
	}

}
